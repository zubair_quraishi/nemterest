(defproject org.clojars.zubairq/coils "nemterest-0.7.5-beta"
  :dependencies [
                 [org.clojure/clojure "1.7.0" :scope "provided"]
                 [org.clojure/clojurescript "1.7.170" :scope "provided"]
                 [org.omcljs/om "1.0.0-alpha22"]
                 [org.clojure/core.async "0.2.374" :scope "provided"]
                 [cljsjs/react "0.14.0-0"]

                 [korma "0.4.2"]

                 [org.postgresql/postgresql "9.2-1004-jdbc41"]
                 [org.clojars.gukjoon/ojdbc "1.4"]
                 ; [com.oracle/ojdbc "12.1.0.2"]

                 [compojure "1.4.0"]
                 [ring "1.4.0"]
                 [ring-middleware-format "0.7.0"]
                 [rewrite-clj "0.2.0"]
                 [org.jasypt/jasypt "1.9.2"]
                 [clj-http "2.0.0"]
                 [cheshire "5.5.0"]
                 [ankha "0.1.4"]
                 [overtone/at-at "1.2.0"]
                 [org.clojure/tools.nrepl "0.2.11"]
                 [instaparse "1.4.1"]

                [ring/ring-json "0.4.0"]

                 [org.webjars/codemirror "5.8"]
                 ]
  :repositories {"sonatype-oss-public"
                 "https://oss.sonatype.org/content/groups/public/"}

  :url "http://org.clojars.zubair/coils"

  :jvm-opts ["-Xmx4g"]

  :plugins  [
             [lein-cljsbuild "1.1.1"]
             [lein-httpd "1.0.0"]
             [lein-ring "0.9.7"]
             [lein-figwheel "0.5.0-1"]
             ]

  :profiles {
             :dev
             {
              :figwheel {
                         :websocket-host "localhost"
                         :http-server-root "public" ;; this will be in resources/
                         :ring-handler    webapp.framework.server.core/app
                         :css-dirs ["resources/public"]
                         :on-jsload "webapp.framework.client.main/figwheel-update"
                         }

              :source-paths ["src" "srcfig" "../srcdev"]
              :cljsbuild
              {
               :builds
               [
                {
                 :source-paths ["src" "srcfig" "../srcdev"]
                 :compiler     {
                                ;:preamble       ["public/react.min.js"]
                                :output-to      "resources/public/main.js"
                                :output-dir     "resources/public/out/"
                                :optimizations  :none
                                ;:output-wrapper false
                                ;:externs        ["resources/public/google_maps_api_v3_11.js"]
                                ;:pretty-print   false
                                :cache-analysis true
                                :source-map-timestamp true
                                :source-map true
                                }
                 }
                ]

               }
              }

             :base
             {
              :figwheel {
                         :websocket-host "localhost"
                         :http-server-root "public" ;; this will be in resources/
                         :ring-handler    webapp.framework.server.core/app
                         :css-dirs ["resources/public"]
                         :on-jsload "webapp.framework.client.main/figwheel-update"
                         }

              :source-paths ["src" "srcfig" "srcbase"]
              :cljsbuild
              {
               :builds
               [
                {
                 :source-paths ["src" "srcfig" "srcbase"]
                 :compiler     {
                                ;:preamble       ["public/react.min.js"]
                                :output-to      "resources/public/main.js"
                                :output-dir     "resources/public/out/"
                                :optimizations  :none
                                ;:output-wrapper false
                                ;:externs        ["resources/public/google_maps_api_v3_11.js"]
                                ;:pretty-print   false
                                :cache-analysis true
                                :source-map-timestamp true
                                :source-map true
                                }
                 }
                ]

               }
              }


             :test
             {
              :id "test"
              :source-paths ["src" "../srctest"]
              :cljsbuild
              {
               :builds
               [
                {
                 :source-paths ["src"]
                 :compiler     {
                                :output-to        "resources/public/main.js"
                                :optimizations     :advanced
                                :preamble         ["resources/public/react.min.js"]
                                :pretty-print      false
                                }
                 }
                ]

               }
              }

             :prod
             {
              :figwheel false
              :source-paths ["src" "../srcprod"]
              :cljsbuild
              {
               :builds
               [
                {
                 :source-paths ["src"]
                 :compiler     {
                                :output-to        "resources/public/main.js"
                                :optimizations     :advanced
                                :preamble         ["resources/public/react.min.js"]
                                :pretty-print      false
                                }
                 }
                ]

               }
              }
             }


  :ring {:init       webapp.server.fns/main-init
         :handler    webapp.framework.server.core/app}


  )
