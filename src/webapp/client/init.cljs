(ns webapp.client.init
  (:require
   [webapp.framework.client.coreclient   :as c     :include-macros true]
   [cljs.core.async                      :refer [put! chan <! pub timeout]]
   )
  (:use
   [webapp.client.react.views.nemterest-main    :only   [nemterest-main-view]]
   [webapp.framework.client.system-globals      :only   [app-state  data-state  set-ab-tests]]
   )
  (:require-macros
   [cljs.core.async.macros :refer [go]]))

(c/ns-coils 'webapp.client.init)



(defn  setup []
  {
   :start-component
   nemterest-main-view

   :setup-fn
   (fn[]
     (do
       (reset!
        app-state

        (assoc-in
         @app-state [:ui]
         {
          :cvs {
                :search {:value ""}
                }



          :edit-cvs
          {
           }


          :secret false
          :tab "rate"



          :login
          {
           :from-email {
                        :value ""}
           :new-member true
           }


          }))


	   (reset! data-state
			   {
				:submit
				{
				 }



				:data-sources
				{
				 :cvs {
					   :def {
							 :type "dbtable"
							 :table "ojobs_cvs"
							 :primary-key
							 {
							  :field :id
							  :type "UUID"
							  }



							 :fields
							 {
							  :id nil
							  :profession nil
							 ; :jobs
							 ; {
							 ;  :type "foreign one to many"
							 ;  :source :jobs
							 ;  :foreign-field-id :fk-cv-id
							 ;  }
							 ; :count-jobs
							 ; {
							 ;  :type "count"
							 ;  :source :THIS
							 ;  :field :jobs
							 ;  }

							  :user_profile  nil
							  :user_picture  nil
							  }




							 :allow-inserts false
							 :allow-updates true
							 }}





         :ojobs_cvs {
                :def {
                      :type   "dbtable"
                      :table "ojobs_cvs"
                      :primary-key
                              {
                               :field :id
                               :type "UUID"
                               }
                      :fields {
                               :id nil
                               }
                      }
                }


         :jobs {
                :def {
                      :type   "dbtable"
                      :table "ojobs_user_jobs"
                      :primary-key
                      {
                       :field :id
                       :type "UUID"
                       }
                      :fields {
                               :id nil
                               :company nil
                               :position nil
                               }
                      }
                }


         :assignments {
                       :def {
                             :type   "dbtable"
                             :table "ojobs_job_assignments"
                             :primary-key
                             {
                              :field :id
                              :type "UUID"
                              }
                             :fields {
                                      :id nil
                                      :responsibility nil
                                      :achievements nil
                                      }
                             }
                       }



         :skills {
                       :def {
                             :type   "dbtable"
                             :table "ojobs_skills"
                             :primary-key
                                     {
                                      :field :id
                                      :type "UUID"
                                      }
                             :fields {
                                      :id nil
                                      }
                             }
                       }

         :languages {
                  :def {
                        :type   "dbtable"
                        :table "ojobs_languages"
                        :primary-key
                                {
                                 :field :id
                                 :type "UUID"
                                 }
                        :fields {
                                 :id nil
                                 }
                        }
                  }



         :ojobs_overall_cv_ratings_by_user {
                     :def {
                           :type   "dbtable"
                           :table "ojobs_overall_cv_ratings_by_user"
                           :primary-key
                                   {
                                    :field :id
                                    :type "UUID"
                                    }
                           :fields {
                                    :id nil
                                    }
                           }
                     }




         }

				})


	   (set-ab-tests {
					  })
	   ))})



