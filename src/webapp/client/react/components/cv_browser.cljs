(ns webapp.client.react.components.cv-browser
  (:require
   [webapp.framework.client.coreclient   :as c :include-macros true]
   [om.core          :as om :include-macros true]
   [cljs.core.async  :refer [put! chan <! pub timeout]]
   [clojure.data     :as data]
   [clojure.string   :as string]
   [ankha.core       :as ankha])
  (:require-macros
    [cljs.core.async.macros :refer [go]])
  (:use-macros
   [webapp.framework.client.coreclient  :only [ns-coils sql log sql-1 log
                                               watch-data  -->ui  <--data <--ui
                                               watch-ui
                                               remote  defn-ui-component
                                               container  map-many  inline  text
                                               div   img pre
                                               component h2 input
                                               write-ui read-ui container
                                               inline text admin
                                               ==data
                                               ==ui  -->ui  watch-ui <--ui
                                               <--data -->data
                                               remote
                                               input
                                               component <--
                                               h1 h2 h3 h4 h5 h6
                                               span  data-view-v2
                                               watch-data map-many inline text
                                               container <--pos <--id
                                               session-user-id data-view-v2 select dselect realtime drealtime data-view-result-set
                                               ]]))
(ns-coils 'webapp.client.react.components.cv-browser)





(def default-width    260)
(def records-per-column    2)
(def row-height   250)






(defn get-month-as-str [n]
  (case n
    1 "Jan"
    2 "Feb"
    3 "Mar"
    4 "Apr"
    5 "May"
    6 "Jun"
    7 "Jul"
    8 "Aug"
    9 "Sep"
    10 "Oct"
    11 "Nov"
    12 "Dec"
    ""
    )
  )











(defn-ui-component     component-cv-search [ui]
  (div {:style {:width "200px" :padding "20px" :height "50px" }}
   (input {
           :value (str (read-ui  ui [ :search :value]))
           :style {:fontSize "14px" }
           :onChange (fn [event]
                     (do
                        ;(write-ui  ui  [:admins :start]  1)
                        (write-ui  ui  [:top]    true)
                        (write-ui  ui  [:search :value]  (.. event -target -value ))
                     ))
           } "")))













(defn-ui-component     component-cv-preview [cv]

  (let [profession (read-ui cv [:value :profession] )
        cv-points  (read-ui cv [:value :zgen_points] )
        cv-id      (read-ui cv [:value :id])
        ;cv-points2 (select zgen_points from ojobs_cvs where id = ? {:params [cv-id]})
        ;cv-points3 (get (first cv-points2) :zgen_points)
        ;cv-points  (if cv-points3 cv-points3 0)
        ]
    (div {:style {  :width "200px" :height "200px" :overflow "hidden"}
          :onClick (fn [event]
                     (do
                       ;(log (pr-str "clicked on: " (-> cv :value :id)))
                       (write-ui cv [:clicked] true) ))                    }

         (div {:style         {:fontSize "14px" :fontWeight "bold"
                               :color (cond (pos? cv-points) "green" (neg? cv-points) "red" :else "black")    }

               :onTouchStart  (fn [event]
                                (write-ui cv [:clicked] true))     }

              (str (read-ui cv [:value :profession] ) " " cv-points))

         (container

          (inline "50%"
                  (div {:style {:fontSize "14px" :width "90%"}
                        }
                       (read-ui cv [:value :user_profile])))

          (inline "50%"
                   (div {:style {:fontSize "14px" :width "90%"
                                 :verticalAlign "top"}}

                        (if (read-ui cv [:value :user_picture])
                          (img {:className   "rcorners1"
                                :style      {:width         "100%"
                                             :verticalAlign "top"   }
                                :src    (str "http://www.nemcv.com/images/ojobs/" (read-ui cv [:value :user_picture])) })
                       )))))))










(defn-ui-component     component-cv-preview2 [cv]

  (let [profession (read-ui cv [:value :profession] )]
    (div {:style {  :width "200px" :height "200px" :overflow "hidden"}
          } (str path))))













(defn-ui-component     component-assignments [job]
  (div {:style {:width "90%"}}
  (data-view-v2
                 {
                  :data-source   :assignments
                  :db-table       "ojobs_job_assignments"
                  ;:relative-path []
                  :fields        [:id :responsibility :achievements]
                  :where         "fk_ojobs_user_job_id = ?"
                  :params        [ (read-ui job [:value :id]) ]

                  }

                 {  :start  1
                    :end    1  }

                 (div {}
                 (div {:style { :paddingTop "20px"}}
                      (container
                        (div {:style {:fontSize "14px" :textAlign "left"
                                      :width "18%" :display "inline-block;"
                                      :paddingLeft "20px"}} "Responsibility" )
                        (div {:style {:whiteSpace "pre-wrap" :fontSize "14px"
                                      :textAlign "left" :verticalAlign "top"
                                      :width "82%" :display "inline-block;" :paddingLeft "20px"}}
                             (str   (<-- :responsibility)  ) )
                        ))

                      (if (pos? (count (<-- :achievements)))
                 (div {:style { :paddingTop "20px"}}
                      (container
                        (div {:style {:fontSize "14px" :textAlign "left"
                                      :width "18%" :display "inline-block;"
                                      :paddingLeft "20px"}} "Achievements" )
                        (div {:style {:whiteSpace "pre-wrap" :fontSize "14px"
                                      :textAlign "left" :verticalAlign "top"
                                      :width "82%" :display "inline-block;" :paddingLeft "20px"}}
                             (str   (<-- :achievements)  ) )
                        )))
                 ))))












(defn-ui-component     component-show-cv-details [cv]
  (let [
        profession       (read-ui cv [:value :profession] )
        ]


             (div {}
                   (div {:style   {:fontSize "30px"
                                   :fontWeight "bold"
                                   :fontFamily "Arial"
                                   :textAlign "left"
                                   :marginTop "20px"
                                   :color "black"
                                   }} "Curriculum Vitae")

                   (div {:style   {:marginTop "20px"
                                   :color "black"
                                   :marginRight "5%"
                                   :border "solid lightgray 1px"
                                   }} "")
                   (div {:style   {:marginTop "0px"
                                   :marginRight "5%"
                                   :color "black"
                                   :border "solid blue 5px"
                                   }} "")



                   (div {:style {:padding "20px"}
                         ;:onClick (fn [event] (if cv (write-ui cv [:clicked] true)))
                         }
                        )
                   (container

                    (inline "60%"

                            (div {:className  "rcorners1"
                                  :style   {:marginTop "0px"
                                            :paddingLeft "10px"
                                            :color "black"
                                            :textAlign "left"
                                            :fontSize "24px"
                                            :background "lightGray"
                                            :border "solid lightgray 1px"
                                            :marginBottom "10px"
                                            }} "Candidate")

                            (div {:style   {:fontSize "18px" :fontWeight "bold"
                                            :marginTop "20px" :textAlign "left"
                                            :paddingLeft "20px"
                                            }
                                  } (str  "Name (hidden for public viewing)"))

                            (div {:style   {:fontSize "14px"
                                            :paddingBottom "40px"
                                            :paddingLeft "20px"
                                            :marginBottom "20px" :textAlign "left"
                                            }
                                  } (str profession ))

                            (div {:className  "rcorners1"
                                  :style   {:marginTop "20px"
                                            :color "black"
                                            :paddingLeft "10px"
                                            :textAlign "left"
                                            :fontSize "24px"
                                            :background "lightGray"
                                            :border "solid lightgray 1px"
                                            :marginBottom "10px"
                                            }} "Profile")


                           (div {:style {:whiteSpace "pre-wrap"
                                         :paddingLeft "20px"
                                          :fontSize "14px" :width "90%" :textAlign "left"}}
                                 (read-ui cv [:value :user_profile]))

                            )

                    (inline "40%"
                            (div {
                                  :style {:width "90% "
                                          :verticalAlign "top"
                                          :margin "0%"}}
                                 (if (read-ui cv [:value :user_picture])
                                   (img {:className "rcorners2"
                                         :style {:width "90%"
                                                 :verticalAlign "top"}
                                         :src
                                         ;(str "http://127.0.0.1/images/ojobs/4e5e607e-da73-4982-9024-75a8b3a53449.JPG?time=1415980028403" )})

                                         (str "http://www.nemcv.com/images/ojobs/" (read-ui cv [:value :user_picture])) })
                                   )

                                 (div {:style   {:fontSize "14px" :fontWeight "bold"
                                                 :marginTop "20px" :textAlign "left"
                                                 :marginLeft "20px"                                                 }
                                       } (str  "Contact"))

                                 (div {:style   {:fontSize "14px"  :textAlign "left" :marginLeft "20px" }}
                                       (str  "The address has been hidden"))
                                 (div {:style   {:fontSize "14px"  :textAlign "left" :marginLeft "20px" }}
                                      (str  "for public viewing. This is good, as it"))
                                 (div {:style   {:fontSize "14px"  :textAlign "left" :marginLeft "20px" }}
                                      (str  "shows that here at NemCV we take"))
                                 (div {:style   {:fontSize "14px"  :textAlign "left" :marginLeft "20px" }}
                                      (str  "your privacy seriously"))


                                 (div {:style   {:fontSize "14px" :fontWeight "bold"
                                                 :marginTop "20px" :textAlign "left"
                                                 :marginLeft "20px"                                                 }
                                       } (str  "Date and place of birth"))
                                 (div {:style   {:fontSize "14px"  :textAlign "left" :marginLeft "20px" }}
                                      (str  "Hidden"))


                                 (div {:style   {:fontSize "14px" :fontWeight "bold"
                                                 :marginTop "20px" :textAlign "left"
                                                 :marginLeft "20px"                                                 }
                                       } (str  "Married status/children"))
                                 (div {:style   {:fontSize "14px"  :textAlign "left" :marginLeft "20px" }}
                                      (str  "Hidden"))




                                 (div {:style   {:fontSize "14px" :fontWeight "bold"
                                                 :marginTop "20px" :textAlign "left"
                                                 :marginLeft "20px"                                                 }
                                       } (str  "Citizenship"))
                                 (div {:style   {:fontSize "14px"  :textAlign "left" :marginLeft "20px" }}
                                      (str  "Hidden"))


                                 (div {:style   {:fontSize "14px" :fontWeight "bold"
                                                 :marginTop "20px" :textAlign "left"
                                                 :marginLeft "20px"                                                 }
                                       } (str  "Personal website"))
                                 (div {:style   {:fontSize "14px"  :textAlign "left" :marginLeft "20px" }}
                                      (str  "Hidden"))



                                 ))
                    )
                   ( div {:style {  :left "0px" :height "40px" :borderTop "40px"}} "")
















                   (div {:className  "rcorners1"
                         :style   {:marginTop "0px"
                                   :paddingLeft "10px"
                                   :marginRight "5%"
                                   :color "black"
                                   :textAlign "left"
                                   :fontSize "24px"
                                   :background "lightGray"
                                   :border "solid lightgray 1px"
                                   :marginBottom "10px"
                                   }} "Work experience summary")


                   (container
                     (div {:style {:fontSize "14px" :textAlign "left" :fontWeight "Bold" :width "25%" :display "inline-block;" :paddingLeft "20px"}} (str  "Date"))
                     (div {:style {:fontSize "14px" :textAlign "left" :fontWeight "Bold" :width "39%" :display "inline-block;"}} (str  "Position"))
                     (div {:style {:fontSize "14px" :textAlign "left" :fontWeight "Bold" :width "36%" :display "inline-block;"}} (str  "Company"))

                     )

                   ( div {:style {  :left "0px" :width "100%" :textAlign "left"}}
                        ( data-view-v2
                          {
                           :data-source   :jobs
                           :db-table       "ojobs_user_jobs"
                           ;:relative-path [:jobs2]
                           :fields        [:id :position :company
                                           :job_start_month :job_start_year
                                           :job_end_month :job_end_year
                                           ]
                           :where         "fk_ojobs_user_id = ?"
                           :params        [ (read-ui cv [:value :id]) ]
                           :order         "job_start_year DESC , job_start_month DESC"
                           }

                          {  :start  1
                             :end    20  }

                          (div {}
                               (div {:className  "rcorners1"
                                     :style   {:marginTop "0px"
                                               :width "90%"
                                              :color "lightGray"
                                               :textAlign "center"
                                               :background "lightGray"
                                               :border "solid lightgray 1px"
                                               :marginLeft "20px"
                                               :marginBottom "10px"
                                               }} "")
                               (container
                           (div {:className  "rcorners1"
                                  :style   {:verticalAlign "top" :fontSize "14px" :textAlign "left" :width "25%" :display "inline-block;" :paddingLeft "20px"}
                                 } (str (get-month-as-str (<-- :job_start_month)) " " (<-- :job_start_year)
                                                    " - "
                                                    (get-month-as-str (<-- :job_end_month)) " " (<-- :job_end_year)
                                                    ))
                           (div {:className  "rcorners1"
                                 :style   {:verticalAlign "top" :fontSize "14px" :textAlign "left" :width "39%" :display "inline-block;" }
                                 } (str  (<-- :position)))
                           (div {:className  "rcorners1"
                                 :style   {:verticalAlign "top" :fontSize "14px" :textAlign "left" :width "29%" :display "inline-block;" }
                                 } (str  (<-- :company)))
                           (div {:className  "rcorners1"
                                 :style   {:verticalAlign "top" :fontSize "14px" :textAlign "left" :width "7%" :display "inline-block;" }
                                 } "" )

                           ))))
                   (div {:style {  :left "0px" :height "50px" :borderTop "60px"}} "")






              (if (> (count (read-ui cv [:value :education])) 0)
                (div nil
                   (div {:className  "rcorners1"
                         :style   {:marginTop "0px"
                                   :paddingLeft "10px"
                                   :color "black"
                                   :textAlign "left"
                                   :marginRight "5%"
                                   :fontSize "24px"
                                   :background "lightGray"
                                   :border "solid lightgray 1px"
                                   :marginBottom "10px"
                                   }} "Education")

                   (div {:style {:whiteSpace "pre-wrap"
                                 :paddingLeft "20px"
                                 :fontSize "14px" :width "90%" :textAlign "left"}}
                        (read-ui cv [:value :education]))
                   ( div {:style {  :left "0px" :height "60px" :borderTop "60px"}} "")
                     ))


               (if (> (count (read-ui cv [:value :certifications])) 0)
                 (div nil

                   (div {:className  "rcorners1"
                         :style   {:marginTop "0px"
                                   :paddingLeft "10px"
                                   :color "black"
                                   :textAlign "left"
                                   :marginRight "5%"
                                   :fontSize "24px"
                                   :background "lightGray"
                                   :border "solid lightgray 1px"
                                   :marginBottom "10px"
                                   }} "Certifications")

                   (div {:style {:whiteSpace "pre-wrap"
                                 :paddingLeft "20px"
                                 :fontSize "14px" :width "90%" :textAlign "left"}}
                        (read-ui cv [:value :certifications]))
                   ( div {:style {  :left "0px" :height "60px" :borderTop "60px"}} "")
                      )
                 )







                   (div {:className  "rcorners1"
                         :style   {:marginTop "0px"
                                   :paddingLeft "10px"
                                   :color "black"
                                   :textAlign "left"
                                   :marginRight "5%"
                                   :fontSize "24px"
                                   :background "lightGray"
                                   :border "solid lightgray 1px"
                                   :marginBottom "10px"
                                   }} "Skills")

                   (data-view-v2
                     {
                      :data-source   :skills
                      :db-table       "ojobs_skills"
                      :relative-path [:skills]
                      :fields        [:id :skill_name :skill_text]
                      :where         "fk_ojobs_user_id = ?"
                      :params        [ (read-ui cv [:value :id]) ]
                      :order         "skill_index ASC"
                      }

                     {  :start  1
                      :end    20  }

                     (div {:style { :paddingTop "20px"}}
                          (container
                            (div {:style {:fontSize "14px" :textAlign "left" :fontWeight "Bold"
                                          :width "30%" :display "inline-block;"
                                          :paddingLeft "20px"}} (str (<-- :skill_name)) )
                            (div {:style {:whiteSpace "pre-wrap" :fontSize "14px"
                                          :textAlign "left" :verticalAlign "top"
                                          :width "70%" :display "inline-block;" :paddingLeft "20px"}} (str (<-- :skill_text)) )
                            )))

                   ( div {:style {  :left "0px" :height "60px" :borderTop "60px"}} "")














                   (div {:className  "rcorners1"
                         :style   {:marginTop "0px"
                                   :paddingLeft "10px"
                                   :color "black"
                                   :textAlign "left"
                                   :marginRight "5%"
                                   :fontSize "24px"
                                   :background "lightGray"
                                   :border "solid lightgray 1px"
                                   :marginBottom "10px"
                                   }} "Languages")

                   (data-view-v2
                     {
                      :data-source   :languages
                      :db-table       "ojobs_languages"
                      :relative-path [:languages]
                      :fields        [:id :language_name :language_text]
                      :where         "fk_ojobs_user_id = ?"
                      :params        [ (read-ui cv [:value :id]) ]
                      :order         "language_index ASC"
                      }

                     {  :start  1
                      :end    20  }

                     (div {:style { :paddingTop "20px"}}
                          (container
                            (div {:style {:fontSize "14px" :textAlign "left" :fontWeight "Bold"
                                          :width "30%" :display "inline-block;"
                                          :paddingLeft "20px"}} (str (<-- :language_name)) )
                            (div {:style {:whiteSpace "pre-wrap" :fontSize "14px"
                                          :textAlign "left" :verticalAlign "top"
                                          :width "60%" :display "inline-block;"
                                          :paddingLeft "20px"}} (str (<-- :language_text)) )
                            (div {:style {:whiteSpace "pre-wrap" :fontSize "14px"
                                          :textAlign "left" :verticalAlign "top"
                                          :width "10%" :display "inline-block;"
                                          :paddingLeft "20px"}}  )
                            )))

                   ( div {:style {  :left "0px" :height "60px" :borderTop "60px"}} "")













                   (div {:className  "rcorners1"
                         :style   {:marginTop "0px"
                                   :paddingLeft "10px"
                                   :marginRight "5%"
                                   :color "black"
                                   :textAlign "left"
                                   :fontSize "24px"
                                   :background "lightGray"
                                   :border "solid lightgray 1px"
                                   :marginBottom "10px"
                                   }} "Work experience")




                   (div {:style {  :left "0px" :width "100%" }}
                        (data-view-v2
                                       {
                                        :data-source   :jobs
                                        :db-table       "ojobs_user_jobs"
                                        :relative-path [:jobs_details]
                                        :fields        [:id :company :position
                                                        :job_start_month :job_start_year
                                                        :job_end_month :job_end_year
                                                        ]
                                        :where         "fk_ojobs_user_id = ?"
                                        :params        [ (read-ui cv [:value :id]) ]
                                        :order         "job_start_year DESC , job_start_month DESC"
                                       }

                                       {  :start  1
                                          :end    20  }

                                       (div {:style { :paddingTop "20px"}}
                                            (div {:className  "rcorners1"
                                                  :style   {:marginTop "0px"
                                                            :paddingTop "5px"
                                                            :paddingBottom "5px"
                                                            :paddingLeft "10px"
                                                            :marginRight "5%"
                                                            :color "black"
                                                            :textAlign "left"
                                                            :fontSize "15px"
                                                            :background "lightGray"
                                                            :border "solid lightgray 1px"
                                                            :marginBottom "10px"
                                                            }}
                                              (div {:style {:display "inline-block;"
                                                            :width   "25%"
                                                            :verticalAlign "top"
                                                            :textAlign "left"
                                                            }}  (str (get-month-as-str (<-- :job_start_month)) " " (<-- :job_start_year)
                                                                    " - "
                                                                     (get-month-as-str (<-- :job_end_month)) " " (<-- :job_end_year)
                                                                      ))

                                              (inline "3%" (str  ""))

                                              (div {:style {:display "inline-block;"
                                                            :width   "44%"
                                                            :verticalAlign "top"
                                                            :textAlign "left"
                                                            }}  (str  (<-- :position)))
                                             (inline "3%" (str  " "))

                                              (div {:style {:display "inline-block;"
                                                            :width   "25%"
                                                            :verticalAlign "top"
                                                            :textAlign "left"
                                                            }}  (str  (<-- :company)))

                                             )


                                            ;(if (<--id)
                                            (component   component-assignments
                                                         cv
                                                         [:jobs_details :values (<--id)] )

                                            ( div {:style {  :left "0px" :height "60px" :borderTop "60px"}} "")


                                            )


                                       ;)

                                       )))))






















(defn-ui-component     component-show-cv [cv]

  (let [
        total-points-results         (select id, zgen_points from ojobs_cvs where id = ? order by id {:params [(read-ui cv [:value :id] )]} )
        total-points                 (get (first total-points-results) :zgen_points)
        cv-id                        (read-ui cv [:value :id] )
        my-points-on-cv-results      (select  id, zgen_points  from  ojobs_overall_cv_ratings_by_user
                                              where  zgen_user_doing_the_rating = ? and zgen_cv_id_being_rated = ?
                                              {:params   [ (session-user-id)  cv-id ] })
        my-points-on-cv              (get (first my-points-on-cv-results) :zgen_points)
        ]

    (div {:style {:position "fixed" :left "0px" :right "0px" :top "0px" :bottom "0px":margin "0px"}}
         (div {:style   {:margin "0px"}}
              (div {:style   {:position "fixed"  :left "10%"  :right "10%" :top "10%"  :bottom "15%" :margin  "0px"
                              :overflow "auto"}}

                   (div {:onClick (fn [event]
                                    (if (< my-points-on-cv 1)
                                      (go (remote upvote {:id                           cv-id
                                                          :user-id-doing-the-rating    (session-user-id)   }))))}


                        (cond
                         (> my-points-on-cv 0)
                         (img {:style {:verticalAlign "top"} :src  (str "images/upselected.png" ) })

                         :else
                         (img {:style {:verticalAlign "top"} :src  (str "images/upnotselected.png" ) })))




                   (div {:style   {:fontSize "26px" :fontWeight "bold" :color "black"}}
                        (if total-points (str total-points) "0"))






                   (div {:onClick (fn [event]
                                    (if (> my-points-on-cv -1)
                                      (go (remote downvote {:id                           cv-id
                                                            :user-id-doing-the-rating    (session-user-id)   }))))}
                        (cond
                         (< my-points-on-cv 0)
                         (img {:style {:verticalAlign "top"} :src  (str "images/downselected.png" ) })

                         :else
                         (img {:style {:verticalAlign "top"} :src  (str "images/downnotselected.png" ) })))


                   (component component-show-cv-details  cv [])





                )))))

















(defn-ui-component   component-show-cv-container [cvs]

  (if (read-ui  cvs [:cv-id])

    (div {:style        {:position        "fixed"
                         :left            "5%"
                         :top             "5%"
                         :width           "90%"
                         :height          "90%"
                         :border          "solid 1px black;"
                         :zIndex          "20000"
                         :backgroundColor "white"
                         :opacity         "1.0"
                         :textAlign       "center"
                         }

          }


         (div {:style { :verticalAlign "center" }}

              (div {:style {:padding "5px" :paddingBottom "30px"}}

                   (component

                    component-show-cv  cvs [:values  (:cv-id  cvs)]) ))


         (div {:style        {:position        "fixed"
                              :left            "5%"
                              :bottom             "5%"
                              :width           "90%"
                              :height          "10%"
                              :border          "solid 1px black;"
                              :zIndex          "20000"
                              :backgroundColor "lightgreen"
                              :opacity         "1.0"
                              :textAlign       "center"
                              :verticalAlign   "middle"
                              :fontSize        "2em"
                              }

               :onTouchStart #(do
                               (c/write-ui cvs [:cv-id] nil)
                               ;(log "clear CV " (c/read-ui  cvs [:cv-id]))
                               )

               :onClick      #(do
                               (c/write-ui cvs [:cv-id] nil)
                               ;(log "clear CV " (c/read-ui cvs [:cv-id]))
                               )
               }
              "Close"
              )


         )))












(defn-ui-component     component-cv-list   [ui]

                       {  :on-mount  (let [dom-element  (om.core/get-node owner)]
                                       (log (pr-str (.-offsetWidth  dom-element)))

                                       )   }

                      (do

                         (if (read-ui  ui [:top])
                           (do
                             (write-ui  ui [:top] false)
                             (set! (.-scrollTop  (. js/document (getElementById "mainlist"))   ) 0)
                             (log (str "scroll top: " (.-scrollTop (. js/document (getElementById "mainlist")))))
                             nil
                             ))





                              (let [
                                    width                   (if (read-ui  ui [:width])
                                                              (read-ui  ui [:width])
                                                              default-width)

                                    records-per-row         (. js/Math floor (/ width default-width))


                                    visible-record-count   (* records-per-column records-per-row)
                                    win-height             (* row-height records-per-column)
                                    win-width              win-height
                                    record-count           (read-ui  ui [:admins :count])
                                    start                  (or (read-ui  ui [:admins :start]) 1)
                                    end                    (or (read-ui  ui [:admins :end])   visible-record-count)
                                    start-row-number       (js/Math.ceil (/ start records-per-row))
                                    end-row-number         (js/Math.floor (/ end   records-per-row))
                                    below-height           (* row-height (dec start-row-number))
                                    last-row               (js/Math.floor (/ record-count records-per-row))
                                    after-height           (* row-height (- last-row end-row-number))
                                   ]
                                ;(log (str "start:" start))
                                ;(log (str "end:" end))
                                ;(log (str "start row:" start-row-number))
                                ;(log (str "end row:" end-row-number))
                                      (div {:style {
                                                    :height      (str win-height "px" )
                                                    :overflowY   "scroll !important"
                                                    }
                                            :id "mainlist"
                                            :onScroll (fn[e]
                                                        (let [
                                                              scroll-top     (.. e -target -scrollTop)
                                                              scroll-height  (js/parseInt (.. e -target -scrollHeight))
                                                              offset-height  (js/parseInt (.. e -target -offsetHeight))
                                                              scroll-row-number     (js/Math.floor (/ scroll-top row-height))
                                                              ]
                                                          (write-ui ui [:admins :start] (inc (* scroll-row-number records-per-row)))
                                                          (write-ui ui [:admins :end]   (* (+ 1 scroll-row-number records-per-column) records-per-row)
                                                                    )))

                                            }
                                           (div {:style {:height (str below-height "px") :border "solid 0px black"}} "")


                                           (data-view-v2
                                             {
                                              :data-source   :cvs
                                              :db-table       "ojobs_cvs"
                                              :relative-path [:admins]
                                              :fields        [:id :user_profile :profession :user_picture
                                                              :education :certifications :zgen_points]
                                              ;:fields        [:id  :profession  :user_profile  :user_picture]
                                              ;:where         "user_type='admin'"
                                              ;:where         "UPPER(profession) like '%JAVA%'"
                                              :where         "lower(profession) like ? and  get_comments = 'YES' "
                                              :params        [ (str "%" (.toLowerCase (str (read-ui ui [:search :value]))) "%") ]
                                              ;:order         "zgen_points  DESC NULLS LAST "
                                              :order         "(zgen_points IS NULL), zgen_points  DESC , id asc "
                                              :realtime     true
                                              }
                                             ;{:start 1 :end 1}
                                             ;{:start 2 :end 2}
                                             ;{:start 3 :end 3}

                                             {:start (or (read-ui  ui [:admins :start]) 1)
                                              :end   (or (read-ui  ui [:admins :end])   20)
                                              }

                                             (div {:style {   :display        "inline-block"
                                                           :width          (str row-height "px")
                                                           :height         (str row-height "px")
                                                           :margin         "0px"
                                                           :verticalAlign "top"      }}

                                                  (inline "250px"
                                                          (component   component-cv-preview
                                                                       ui
                                                                       ;[:admins :values (<--id)]
                                                                       [:admins :values (<-- :id)]
                                                                       )
                                                          )))


                                           (div {:style {:height (str after-height "px") :border "solid 0px black"}} "")








                                      ))))












(defn-ui-component     component-cv-browser   [ui]

  {  :on-mount  (let [dom-element  (om.core/get-node owner)]
                  ;(log (pr-str (.-offsetWidth  dom-element)))
                  (write-ui ui [:width] (.-offsetWidth  dom-element) )

                  )   }

  (div nil

       (component  component-show-cv-container  ui [:admins])

         (div  nil
               (component component-cv-list ui [])

               (container
                (inline "30%"  (text
                                (read-ui  ui [:admins :start])  " - " (read-ui  ui [:admins :end])
                                ))
                (inline "30%"  (text " of " (read-ui  ui [:admins :count])))
                )


               (comment select id, profession from ojobs_cvs where profession like '%Manager%'
                        {:main-params []}


                        (div {} (div {} (<-- :profession) )))




               )))













(watch-ui [:ui :cvs :admins :values]
          "Watch the CVs be clicked"
          (let [
                list-of-items    (into [] (vals (<--ui [:ui :cvs :admins :values])))
                ]
            (do
              (doall (map
                      (fn [item]
                        (do
                          (if (and (= (get item :clicked) true) (-> item :value :id))
                            (do
                              ;(log (str  "Found item in list: ") (-> item :value :id) )
                              (-->ui [:ui :cvs :admins :values (-> item :value :id) :clicked] false)
                              (-->ui [:ui :cvs :admins :cv-id] (-> item :value :id))
                              ))))
                      list-of-items
                      )))))


