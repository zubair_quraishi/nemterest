(ns webapp.client.react.components.edit
  (:require
   [webapp.framework.client.coreclient   :as c :include-macros true]
   [goog.net.cookies                     :as cookie]
   )

  (:use
   [webapp.framework.client.ui-helpers      :only  [basic-input-box]]
   [clojure.string :only [blank?]])
  (:use-macros
   [webapp.framework.client.coreclient  :only [ns-coils
                                               sql
                                               log
                                               defn-ui-component
                                               a
                                               div
                                               write-ui
                                               read-ui
                                               ==data
                                               ==ui  -->ui  watch-ui <--ui
                                               <--data -->data
                                               remote
                                               input
                                               component
                                               h1 h2 h3 h4 h5 h6
                                               span
                                               watch-data
                                               add-many map-many
                                               ]])
  (:require-macros
   [cljs.core.async.macros :refer [go]])
  )

(c/ns-coils 'webapp.client.react.components.edit)






(defn validate-email [email]
  (if (pos? (.indexOf (str email) "@"))
    true
    ))






;------------------------------------------------------------
(defn-ui-component    from-email-field  [ui-data]
  ;------------------------------------------------------------
  (div
   nil
   (div  {:style {:fontSize "15px"
                  :display "inline-block"
                  :marginLeft "20px"}}
         "My e-mail address is:")

   (input {:value (read-ui ui-data [:value])
           :onChange    #(write-ui  ui-data  [:value]  (.. %1 -target -value))
           :style {:fontSize "15px"
                   :display "inline-block"
                   :marginLeft "5px"}} "")

   ))





;------------------------------------------------------------
(defn-ui-component    password-field  [ui-data]
  ;------------------------------------------------------------
  (div {
        :style       {:width "100%" }}

       (input
        {:type        "password"
         :value       (read-ui  ui-data [:password])
         :onChange    #(write-ui  ui-data  [:password]  (.. %1 -target -value))
         :disabled    (if (read-ui  ui-data [:new-member]) "true" "")
         :onClick     #(write-ui  ui-data [:new-member] false)
         :style       {:width "100%"
                       :display "inline-block"
                       :marginLeft "5px"
                       :fontSize "15px"}
         } )
       ))







;------------------------------------------------------------
(defn-ui-component    show-dialog-box  [dialog-data]
  ;------------------------------------------------------------
  (if (read-ui dialog-data [:message])
    (div {:style {:position          "absolute"           :left    "5%"  :top "5%"
                  :width   "90%"
                  :height  "90%"
                  :border            "solid 1px black;"   :zIndex  "2000"
                  :background-color  "white"              :opacity "1.0"
                  :text-align        "center"
                  }
          :onTouchStart #(c/write-ui  dialog-data [:message] nil)
          :onClick      #(c/write-ui  dialog-data [:message] nil)
          }

         (div {:style { :verticalAlign "center" }}
              (div {:style {:padding "5px" :paddingBottom "30px"}} (read-ui dialog-data [:message]) )
))))








;------------------------------------------------------------
(defn-ui-component    show-connection-confirmation-dialog-box  [dialog-data]
  ;------------------------------------------------------------
  (if (get-in dialog-data [:show-connection-confirmation])
    (div {:style {:position          "absolute"           :left    "5%"  :top "5%"
                  :width   "90%"
                  :height  "90%"
                  :border            "solid 1px black;"   :zIndex  "2000"
                  :background-color  "white"              :opacity "1.0"
                  :text-align        "center"
                  }
          :onTouchStart #(c/write-ui  dialog-data [:show-connection-confirmation] false)
          :onClick      #(c/write-ui  dialog-data [:show-connection-confirmation] false)
          }

         (div {:style { :verticalAlign "center" }}
              (div {:style {:padding "5px" :paddingBottom "30px"}} "You have now joined the NemCV network!")

              (div {:style {:padding "5px"}} (str " "
                                                  (get-in dialog-data [:from-email :value] )
                                                  ))))))









;------------------------------------------------------------
(defn-ui-component  edit-my-cvs   [ui-data]
  ;------------------------------------------------------------

  (div
   nil
   (map-many
    (fn [item]
      (div nil (str (get-in item [:value :profession])))
      )
    ""
    )
   ))








(==ui [:ui :login :clicked] true
      "When the login button is clicked"

      (do
        ;(-->ui [:user :pretend-to-be-logged-in] true)
        (-->ui [:ui :login :clicked] false)


        (cond

         (= (<--ui [:ui :login :new-member]) true)
         (do
           (-->data [:remote :new-member :from-email :value]
                    (<--ui [:ui :login :from-email :value]))
           (-->data [:remote :new-member :submit] true))




         (= (<--ui [:ui :login :new-member]) false)
         (do
           (-->data [:remote :sign-in :from-email :value]
                    (<--ui [:ui :login :from-email :value]))

           (-->data [:remote :sign-in :password :value]
                    (<--ui [:ui :login :password]))

           (-->data [:remote :sign-in :submit] true)
         ))))





(==data [:remote :sign-in :submit] true
        "If a request to sign should be made"

        (go
         (-->data [:remote :sign-in :submit] false)
         (-->data [:remote :notifications :sign-in] "waiting")

         (let [resp (remote
                     login-user
                     {
                      :from-email (<--data [:remote :sign-in :from-email :value])
                      :password (<--data [:remote :sign-in :password :value])
                      })]

           (cond
            (resp :error)
            (-->data [:remote :sign-in :result]  resp)

            :else
            (do
              (-->data [:session]  (-> resp ))
              (cookie/set "nemcv.com" (-> resp :session-id) 7200000)
              )
            ))))


(==data [:remote :sign-in :result :error] true
        "When there is an error signing in"
        (do
          (-->data [:remote :sign-in :result :error] nil)
          (-->ui [:ui :login :message] "Error signing in")
          ))

(==data [:remote :new-member :submit] true
        "If a request to join should be made"

        (go
         (-->data [:remote :new-member :submit] false)
         (-->data [:remote :notifications :new-member] "waiting")

         (let [resp (remote
                     submit-email
                     {
                      :from-email (<--data [:remote :new-member :from-email :value])
                      })]

           (cond
            (resp :error)
            (-->data [:remote :new-member :error]  (pr-str resp))

            :else
            (do
              (-->data [:session :user]  (-> resp ))
              (-->data [:session :endorsement-id]  (-> resp :value :endorsement_id))
              )
            )))
        )


(watch-data [:session]
            "When someone logs in from the back end then tell the UI"
            ;(. Console log  ":session")
            (-->ui [:session :logged-in] true)
            (-->ui [:session :user_name] (<--data [:session :user :user_name]))
            )





















(==ui  [:ui  :login  :from-email  :mode]   "validate"
       "Validate the email address whenever it goes into validation mode"


    (cond

      (validate-email (<--ui [:ui :login :from-email :value]))
        (-->ui [:ui :login :from-email :error] "")

      :else
        (-->ui [:ui :login :from-email :error] "Invalid email")
    ))









(watch-ui [:ui :login :from-email :value]
          "When the from email address text is changed check stuff"

          (do
            (if (= (<--ui [:ui :login :from-email :mode]) "validate")

              (if (validate-email  (<--ui [:ui :login :from-email :value]))

                (-->ui [:ui :login :from-email :error] "")
                (-->ui [:ui :login :from-email :error] "Invalid email")
                ))))




(watch-ui [:ui :login :from-email :value]
          "When the from email address text is changed check more stuff"

          (do
            ;(js/alert (<--ui [:ui :login :from-email :value]))
            (if
              (validate-email  (<--ui [:ui :login :from-email :value]))
              (do
                ;(js/alert "valid!")
                (-->ui [:ui :login :details-valid] true)
                ))))

