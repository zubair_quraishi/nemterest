(ns webapp.client.react.components.login
  (:require
   [webapp.framework.client.coreclient   :as c :include-macros true]
   [goog.net.cookies                     :as cookie]
   )

  (:use
   [webapp.framework.client.ui-helpers      :only  [basic-input-box]]
   [clojure.string :only [blank?]])
  (:use-macros
   [webapp.framework.client.coreclient  :only [ns-coils
                                               sql
                                               log
                                               defn-ui-component
                                               a
                                               div
                                               write-ui
                                               read-ui
                                               ==data
                                               ==ui  -->ui  watch-ui <--ui
                                               <--data -->data
                                               remote
                                               input
                                               component <--
                                               h1 h2 h3 h4 h5 h6
                                               span data-view-v2
                                               watch-data map-many inline text
                                               container <--pos
                                               ]])
  (:require-macros
   [cljs.core.async.macros :refer [go]])
  )

(c/ns-coils 'webapp.client.react.components.login)





(def rec-count    15)
(def row-height   30)
(def win-height   (* row-height rec-count))


(defn be-pos [x y]
  (if (pos? x) x y))





(defn-ui-component     component-cv-browser   [ui]
  (let [
        record-count    (read-ui  ui [:admins :count])
        start           (or (read-ui  ui [:admins :start]) 1)
        end             (or (read-ui  ui [:admins :end])   rec-count)
        below-height    (* row-height (- start 1))
        after-height    (* row-height (- record-count end))
        ]
  (div  nil
  (div {:style {
                :height     (str win-height "px" )
                :overflowY   "scroll !important"
                }
        :onScroll (fn[e]
                    (let [
                          scroll-top     (.. e -target -scrollTop)
                          scroll-height  (js/parseInt (.. e -target -scrollHeight))
                          offset-height  (js/parseInt (.. e -target -offsetHeight))
                          rec-number     (inc (js/Math.floor (/ scroll-top row-height)))
                          ]
                         (write-ui ui [:admins :start] rec-number)
                         (write-ui ui [:admins :end] (+ rec-number rec-count))
                      )
                    )
        }
       (div {:style {:height (str below-height "px") :border "solid 0px black"}} "")
            (data-view-v2
                           {
                            :data-source   :cvs
                            :relative-path [:admins]
                            :path          []
                            :fields        [:id  :profession  :user_profile  :user_picture ]
                            :ui-state      ui
                            ;:where         "user_type='admin'"
                            ;:where         "UPPER(profession) like '%JAVA%'"
                            :where         "profession is not null"
                            }

                           ;{:start 1 :end 1}
                           ;{:start 2 :end 2}
                           ;{:start 3 :end 3}

                           {:start start
                            :end   end
                            }


                           (div {:style {
                                         :display        "inline-block"
                                         :width          "95%"
                                         :height         (str row-height "px")
                                         :margin         "0px"
                                         :verticalAlign "top"
                                         }}
                                (inline "7%"  (text (<--pos) ))
                                (inline "93%" (text (<-- :profession) ))
                                ))
       (div {:style {:height (str after-height "px") :border "solid 0px black"}} "")

       )
       (container
        (inline "30%"  (text
                        (read-ui  ui [:admins :start])  " - " (read-ui  ui [:admins :end])
                        ))
        (inline "30%"  (text " of " (read-ui  ui [:admins :count])))
        ))))










































(def rec-count2    40)
(def row-height2   30)
(def win-height2   400)



(defn-ui-component     component-cv-browser2   [ui]
  (let [
        record-count    (read-ui  ui [:admins :count])
        row-height      30
        window-height   (* record-count row-height)
        ]
  (div  nil
  (div {:style {
                :height (str win-height2 "px" )
                :overflowY "scroll"
                }
        :onScroll (fn[e]
                    (let [
                          scroll-top     (.. e -target -scrollTop)
                          scroll-height  (js/parseInt (.. e -target -scrollHeight))
                          offset-height  (js/parseInt (.. e -target -offsetHeight))
                          ]
                      ;(js/alert (pr-str record-count))
                      (cond
                       (= scroll-top 0)
                       (do
                         (write-ui ui [:admins :start] (be-pos (- (read-ui  ui [:admins :start]) rec-count2) 1))
                         ;(write-ui ui [:admins :end] (be-pos (- (read-ui  ui [:admins :end]) rec-count2) rec-count2))
                         ;(set! (.. e -target -scrollTop) (- win-height 10))
                         )
                       (>= (+ offset-height scroll-top) scroll-height)
                        (do
                          ;(write-ui ui [:admins :start] (+ (read-ui  ui [:admins :start]) rec-count))
                          (write-ui ui [:admins :end] (+ (read-ui  ui [:admins :end]) rec-count))
                          ;(set! (.. e -target -scrollTop) 0)
                          )
                        )
                      )
                    )
        }
            (data-view-v2
                           {
                            :data-source   :cvs
                            :relative-path [:admins]
                            :path          []
                            :fields        [:id  :profession  :user_profile  :user_picture]
                            :ui-state      ui
                            ;:where         "user_type='admin'"
                            ;:where         "UPPER(profession) like '%JAVA%'"
                            :where         "profession is not null"
                            }

                           ;{:start 1 :end 1}
                           ;{:start 2 :end 2}
                           ;{:start 3 :end 3}

                           {:start (or (read-ui  ui [:admins :start]) 1)
                            :end   (or (read-ui  ui [:admins :end])   rec-count)
                            }


                           (div {:style {
                                         :display        "inline-block"
                                         :width          "100%"
                                         :margin         "0px"
                                         :verticalAlign "top"
                                         }}
                                (inline "5%"  (text (<--pos) ))
                                (inline "95%" (text (<-- :profession) ))
                                )))
       (container
        (inline "30%"  (text
                        (read-ui  ui [:admins :start])  " - " (read-ui  ui [:admins :end])
                        ))
        (inline "30%"  (text " of " (read-ui  ui [:admins :count])))
        ))))




(defn validate-email [email]
  (if (pos? (.indexOf (str email) "@"))
    true
    ))






;------------------------------------------------------------
(defn-ui-component    from-email-field  [ui-data]
  ;------------------------------------------------------------
  (div
   nil
   (div  {:style {:fontSize "15px"
                  :display "inline-block"
                  :marginLeft "20px"}}
         "My e-mail address is:")

   (input {:value (read-ui ui-data [:value])
           :onChange    #(write-ui  ui-data  [:value]  (.. %1 -target -value))
           :style {:fontSize "15px"
                   :display "inline-block"
                   :marginLeft "5px"}} "")

   ))





;------------------------------------------------------------
(defn-ui-component    password-field  [ui-data]
  ;------------------------------------------------------------
  (div {
        :style       {:width "100%" }}

       (input
        {:type        "password"
         :value       (read-ui  ui-data [:password])
         :onChange    #(write-ui  ui-data  [:password]  (.. %1 -target -value))
         :disabled    (if (read-ui  ui-data [:new-member]) "true" "")
         :onClick     #(write-ui  ui-data [:new-member] false)
         :style       {:width "100%"
                       :display "inline-block"
                       :marginLeft "5px"
                       :fontSize "15px"}
         } )
       ))







;------------------------------------------------------------
(defn-ui-component    show-dialog-box  [dialog-data]
  ;------------------------------------------------------------
  (if (read-ui dialog-data [:message])
    (div {:style {:position          "absolute"           :left    "5%"  :top "5%"
                  :width   "90%"
                  :height  "90%"
                  :border            "solid 1px black;"   :zIndex  "2000"
                  :background-color  "white"              :opacity "1.0"
                  :text-align        "center"
                  }
          :onTouchStart #(c/write-ui  dialog-data [:message] nil)
          :onClick      #(c/write-ui  dialog-data [:message] nil)
          }

         (div {:style { :verticalAlign "center" }}
              (div {:style {:padding "5px" :paddingBottom "30px"}} (read-ui dialog-data [:message]) )
))))








;------------------------------------------------------------
(defn-ui-component    show-connection-confirmation-dialog-box  [dialog-data]
  ;------------------------------------------------------------
  (if (get-in dialog-data [:show-connection-confirmation])
    (div {:style {:position          "absolute"           :left    "5%"  :top "5%"
                  :width   "90%"
                  :height  "90%"
                  :border            "solid 1px black;"   :zIndex  "2000"
                  :background-color  "white"              :opacity "1.0"
                  :text-align        "center"
                  }
          :onTouchStart #(c/write-ui  dialog-data [:show-connection-confirmation] false)
          :onClick      #(c/write-ui  dialog-data [:show-connection-confirmation] false)
          }

         (div {:style { :verticalAlign "center" }}
              (div {:style {:padding "5px" :paddingBottom "30px"}} "You have now joined the NemCV network!")

              (div {:style {:padding "5px"}} (str " "
                                                  (get-in dialog-data [:from-email :value] )
                                                  ))))))









;------------------------------------------------------------
(defn-ui-component  login   [ui-data]
  ;------------------------------------------------------------

  (div
   nil

   ;(component  component-cv-browser2 ui-data  [])

   ; connection confirmation
   (if [get-in ui-data [:show-connection-confirmation]]
     (component  show-connection-confirmation-dialog-box  ui-data []))


   ; show any messages for the end user
   (if (read-ui ui-data [:message])
     (component  show-dialog-box  ui-data []))


   (div
    {:style {:border "1px solid" :padding "15px" :marginTop "20px"}}

    (div {:style {:color "rgb(228, 121, 17);"}} (str "Sign in"))



    (h5 {:style {:color "rgb(228, 121, 17);"}} (str "What is your e-mail address?"))
    (component   from-email-field   ui-data [:from-email] )



    (h5 {:style {:color "rgb(228, 121, 17);"}} (str "Do you have a NemCV password?"))
    (div {:style {:display "block" :fontSize "15px" }}
         (input {:type "radio" :name "member" :value "new_member"
                 :checked (if (read-ui ui-data [:new-member]) "true" "")
                 :onClick #(write-ui  ui-data [:new-member] true)
                 :style {
                         :fontSize "15px"
                         :marginLeft "20px"

                         }
                 } "No, I am a new member"))


    (div {:style {:display "block" :fontSize "15px" }}
         (input {:type "radio" :name "member" :value "current_member"
                 :checked (if (read-ui ui-data [:new-member]) "" "true")
                 :onClick #(write-ui  ui-data [:new-member] false)
                 :style {:display "inline-block"
                         :fontSize "15px"
                         :marginLeft "20px"
                         }} "Yes, I have a password:")
         (div  {:style       #js {:width "100px" :display "inline-block"}}
               (c/component   password-field   ui-data [] )
                   "")
           "")

    (a { :href         "#"
         :className    "btn btn-warning"
         :style        {:marginTop "5px"}
         :onClick     #(do
                         ;(js/alert "clicked")
                         (write-ui  ui-data [:clicked] true))
         :disabled     (if (read-ui  ui-data [:details-valid]) "" "false")
         }
       "Sign in using our secure server"
       (span {:className "glyphicon glyphicon-play"}))





    (if (not (blank?
              (get-in ui-data [:submit :message])))

      (if (not (get-in ui-data [:confirmed]))
        (c/div nil (str "Please check your Inbox for "
                        (-> ui-data :from-email :value) " to confirm your email address")))
      ))))








(comment ==ui [:ui :login :clicked] true
      "When the login button is clicked"

      (do
        ;(-->ui [:user :pretend-to-be-logged-in] true)
        (-->ui [:ui :login :clicked] false)


        (cond

         (= (<--ui [:ui :login :new-member]) true)
         (do
           (-->data [:remote :new-member :from-email :value]
                    (<--ui [:ui :login :from-email :value]))
           (-->data [:remote :new-member :submit] true))




         (= (<--ui [:ui :login :new-member]) false)
         (do
           (-->data [:remote :sign-in :from-email :value]
                    (<--ui [:ui :login :from-email :value]))

           (-->data [:remote :sign-in :password :value]
                    (<--ui [:ui :login :password]))

           (-->data [:remote :sign-in :submit] true)
         ))))





(comment ==data [:remote :sign-in :submit] true
        "If a request to sign should be made"

        (go
         (-->data [:remote :sign-in :submit] false)
         (-->data [:remote :notifications :sign-in] "waiting")

         (let [resp (remote
                     login-user
                     {
                      :from-email (<--data [:remote :sign-in :from-email :value])
                      :password (<--data [:remote :sign-in :password :value])
                      })]

           (cond
            (resp :error)
            (-->data [:remote :sign-in :result]  resp)

            :else
            (do
              (-->data [:session]  (-> resp ))
              (cookie/set "nemcv.com" (-> resp :session-id) 7200000)
              )
            ))))


(==data [:remote :sign-in :result :error] true
        "When there is an error signing in"
        (do
          (-->data [:remote :sign-in :result :error] nil)
          (-->ui [:ui :login :message] "Error signing in")
          ))

(==data [:remote :new-member :submit] true
        "If a request to join should be made"

        (go
         (-->data [:remote :new-member :submit] false)
         (-->data [:remote :notifications :new-member] "waiting")

         (let [resp (remote
                     submit-email
                     {
                      :from-email (<--data [:remote :new-member :from-email :value])
                      })]

           (cond
            (resp :error)
            (-->data [:remote :new-member :error]  (pr-str resp))

            :else
            (do
              (-->data [:session :user]  (-> resp ))
              (-->data [:session :endorsement-id]  (-> resp :value :endorsement_id))
              )
            )))
        )


(watch-data [:session]
            "When someone logs in from the back end then tell the UI"
            (-->ui [:session :logged-in] true)
            (-->ui [:session :user_name] (<--data [:session :user :user_name]))
            (-->ui [:session :user_id]   (<--data [:session :user :id]))
            )





















(==ui  [:ui  :login  :from-email  :mode]   "validate"
       "Validate the email address whenever it goes into validation mode"


    (cond

      (validate-email (<--ui [:ui :login :from-email :value]))
        (-->ui [:ui :login :from-email :error] "")

      :else
        (-->ui [:ui :login :from-email :error] "Invalid email")
    ))









(watch-ui [:ui :login :from-email :value]
          "When the from email address text is changed check stuff"

          (do
            (if (= (<--ui [:ui :login :from-email :mode]) "validate")

              (if (validate-email  (<--ui [:ui :login :from-email :value]))

                (-->ui [:ui :login :from-email :error] "")
                (-->ui [:ui :login :from-email :error] "Invalid email")
                ))))




(watch-ui [:ui :login :from-email :value]
          "When the from email address text is changed check more stuff"

          (do
            ;(js/alert (<--ui [:ui :login :from-email :value]))
            (if
              (validate-email  (<--ui [:ui :login :from-email :value]))
              (do
                ;(js/alert "valid!")
                (-->ui [:ui :login :details-valid] true)
                ))))


