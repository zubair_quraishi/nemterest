(ns webapp.client.react.views.nemterest-main
  (:require
   [webapp.framework.client.coreclient   :as c :include-macros true]
   [om.core                              :as om :include-macros true]
   [goog.net.cookies                     :as cookie]
   )
  (:use
   [webapp.client.react.components.cv-browser  :only   [component-cv-search
                                                        component-cv-browser]]
   [webapp.client.react.components.login      :only [login]]
   [webapp.client.react.components.edit      :only [edit-my-cvs]]
   )
  (:require-macros
   [cljs.core.async.macros :refer [go]])
  )

(c/ns-coils 'webapp.client.react.views.nemterest-main)





(c/defn-ui-component     nemterest-main-view   [app]
  {}

  (c/div nil
         (c/container
          (c/inline "50%"
          (c/div nil "NemCV Community"))

          (c/inline "15%"
                    (c/div {:onClick
                            #(c/write-ui app [:ui :tab ] "rate")
                            :style {:fontSize "12px"}
                            } (str "")))

          (c/inline "15%"
                    (c/div {:onClick
                            #(let [news (not (c/read-ui app [:ui :secret ]))]
                               (c/write-ui app [:ui :secret ] news)
                               (go
                                ;(js/alert (str news))
                                (c/remote set-user-secret-mode-for-session-id
                                        {:session-id    (cookie/get "nemcv.com")
                                         :secret-mode   news}))
                               )
                            :style {:fontSize "12px"}
                            } (str "secret "
                                   (if (c/read-ui app [:ui :secret ]) "ON" "OFF")
                                   )))

          (c/inline "10%"
                    (c/div {:onClick
                            #(c/write-ui app [:ui :tab ] "edit")
                            :style {:fontSize "12px"}
                            } (str "edit")))

          (c/inline "10%"(if (c/read-ui  app [:session] )
            (c/div {:onClick
                    #(do
                       (cookie/remove "nemcv.com")
                       (c/write-ui app [:session] nil)
                       )
                    :style {:fontSize "12px"}
                    } (str "Logout"
                           ;(c/read-ui  app [:session :user_name] )
                           ))
            )))

         (cond
          (not (c/read-ui  app [:session] ))
           (do
             (c/component  login   app  [:ui :login])
             )

           (= (c/read-ui app [:ui :tab]) "rate")
;         (if (not (c/read-ui  app [:user] ))
           (c/container
             (c/component  component-cv-search  app  [:ui :cvs])
             (c/component  component-cv-browser  app  [:ui :cvs])
             )

           (= (c/read-ui app [:ui :tab]) "edit")
           (do
             (c/component  edit-my-cvs   app  [:ui :edit-cvs])
             )

           )))
