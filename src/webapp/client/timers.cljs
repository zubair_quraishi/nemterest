(ns webapp.client.timers
  (:require
   [goog.net.cookies :as cookie]
   [om.core          :as om :include-macros true]
   [om.dom           :as dom :include-macros true]
   [webapp.framework.client.coreclient   :as  c  :include-macros true]
   [cljs.core.async  :refer [put! chan <! pub timeout]]
   [clojure.data     :as data]
   [clojure.string   :as string]
   [ankha.core       :as ankha]
   )
  (:use
   [webapp.framework.client.system-globals  :only  [app-state
                                                    reset-app-state
                                                    data-state
                                                    add-init-state-fn
                                                    update-data
                                                    touch
                                                    data-session-id
                                                    assoc-in-atom
                                                    ]]
   [clojure.string :only [blank?]]
   )
   (:require-macros
    [cljs.core.async.macros :refer [go]])
  (:use-macros
   [webapp.framework.client.coreclient  :only [ns-coils sql log  sql-1 log]]))






;-------------------------------------------------
;-------------------------------------------------
(def tt (atom 1))





;-------------------------------------------------
;-------------------------------------------------
(defn my-timer []
    (swap! tt inc)
    (c/log (str "Called timer: " @tt)))
;(add-init-state-fn "timer function" #(js/setInterval my-timer 15000))







;-------------------------------------------------
;-------------------------------------------------
(defn process-events []
  (touch [:ui]))
;(add-init-state-fn "process events" #(js/setTimeout process-events 100))
;(add-init-state-fn "get top tests" get-top-tests)







;-------------------------------------------------
;-------------------------------------------------
(defn auto-login []
  (go
   (let [session-id (cookie/get "nemcv.com")]
     (if session-id
       (let [session
             (c/remote  get-user-from-session-id {:session-id session-id})]

         (if (:session-id session)
           (do
             (assoc-in-atom   data-state   [:session]  session)
             (assoc-in-atom   app-state    [:ui :secret]
                                                      (= (:show_secret_mode (:user session)) "Y"))
             (touch [:ui])
             (assoc-in-atom   app-state    [:session :logged-in] true)


           )))))))

(add-init-state-fn "auto login" auto-login)











"-------------------------------------------------
(make-data-session)



This is called to create a data session with the
server. This is called once every time the webapp
is loaded. This enables the server to be able to
track the client.

The server generates the UUID, and then returns
it to the client which then identifies itself by
sending the uuid back to the server whenever it
asks for any data stuff
-------------------------------------------------"
(defn make-data-session []
  (go
   (let [data-session
         (c/remote  create-data-session {})]
     (reset!   data-session-id     (data-session :value))
     (log "Data session ID: "   @data-session-id)
     )))
(add-init-state-fn "make data session" make-data-session)



