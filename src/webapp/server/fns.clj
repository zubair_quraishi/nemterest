(ns webapp.server.fns
  [:require [clojure.string :as str]]
  [:use [webapp.framework.server.systemfns]]
  [:use [webapp.framework.server.email-service]]
  [:use [webapp.framework.server.encrypt]]
  [:use [clojure.repl]]
  [:use [webapp.framework.server.db-helper]]
  [:use [webapp.framework.server.globals]]

  (:use [webapp-config.settings])
  (:use [overtone.at-at])
  (:import [java.util.Date])
  (:import [java.util.UUID])
  (:import [java.util TimerTask Timer])
)











(defn test-server-call
  [ params-passed-in ]
  ;----------------------------------------------------------------
  {:value "Return this to the client"}
  )



(defn main-init []
  {:value "do nothing"}
  )


(defn login-user [{from-email :from-email
                   password   :password}]

  (let [user-request
        (sql-1 "select id, user_name, show_secret_mode from ojobs_users where user_name = ?
             and password = ? "
               ;"and user_type='admin' limit 1"
             [from-email password])
        ]
        (if user-request
          (let [
                session-id (uuid-str)
                ]
            (do
              (sql "insert into ojobs_web_sessions (id, fk_ojobs_user_id) values (?,?)"
                   [session-id, (:id user-request)])
              {
               :session-id   session-id
               :user         user-request
               }
              )
            )
          {:error         true
           :error-message "Could not login user"}
        )
  ))


(defn recalc-cv-ratings
  [{cv-id                       :cv-id
    user-id-doing-the-rating    :user-id-doing-the-rating }]

      (let [final-ratings (sql-1 (str "select id from ojobs_overall_cv_ratings_by_user where "
                                      "zgen_user_doing_the_rating = ? and zgen_cv_id_being_rated = ?")
                                 [user-id-doing-the-rating   cv-id])

            new-points    (get
                            (sql-1 (str "select sum(points) as sum_points from  ojobs_basic_user_ratings  where "
                                        "user_doing_the_rating = ? and cv_id_being_rated = ?")
                                   [user-id-doing-the-rating   cv-id])
                            :sum_points)
            ]

        (if final-ratings
          (sql (str "update  ojobs_overall_cv_ratings_by_user   "
                    "set zgen_points = ? where zgen_user_doing_the_rating = ? and zgen_cv_id_being_rated = ?   ")
               [
                new-points
                user-id-doing-the-rating
                cv-id
                ])

          (sql (str "insert   into  ojobs_overall_cv_ratings_by_user   "
                    "(id, zgen_user_doing_the_rating, zgen_cv_id_being_rated, zgen_points)   "
                    "values (?,?,?,?)")
               [
                (uuid-str)
                user-id-doing-the-rating
                cv-id
                new-points
                ]))





        (let [current-points   (get
                                 (sql-1 (str "select sum(zgen_points) as sum_points from ojobs_overall_cv_ratings_by_user where "
                                                           "zgen_cv_id_being_rated = ?")
                                                      [cv-id])
                                 :sum_points)
              ]
            (sql "update  ojobs_cvs set zgen_points = ? where id = ? " [current-points    cv-id])
            )





          ))





(defn upvote [{id                          :id
               user-id-doing-the-rating    :user-id-doing-the-rating }]




        (sql (str "insert   into   ojobs_basic_user_ratings   "
                  "(id, user_doing_the_rating, cv_id_being_rated, when_rated, points)   "
                  "values (?,?,?,?,?)")
             [
              (uuid-str)
              user-id-doing-the-rating
              id
              (java.sql.Timestamp. (-> (java.util.Date.) .getTime))
              1
              ])


        (recalc-cv-ratings
          {:cv-id                       id
           :user-id-doing-the-rating    user-id-doing-the-rating })


    {}
    )






(defn downvote [{id :id
                 user-id-doing-the-rating    :user-id-doing-the-rating
                 }]


    (sql (str "insert   into   ojobs_basic_user_ratings   "
              "(id, user_doing_the_rating, cv_id_being_rated, when_rated, points)   "
              "values (?,?,?,?,?)")
         [
          (uuid-str)
          user-id-doing-the-rating
          id
          (java.sql.Timestamp. (-> (java.util.Date.) .getTime))
          -1
          ])


  (recalc-cv-ratings
    {:cv-id                       id
     :user-id-doing-the-rating    user-id-doing-the-rating })

    {}
    )









(defn get-user-from-session-id [{session-id :session-id}]

  (let [session-request
        (sql-1 "select id, fk_ojobs_user_id from ojobs_web_sessions where id = ?"
             [session-id])

        user-request
        (sql-1 "select id, user_name, show_secret_mode from ojobs_users where id = ?"
             [(:fk_ojobs_user_id session-request)])
        ]
        (if user-request
          (let [session-id ""]
               {
                :session-id   session-id
                :user         user-request
               }
            )
          {:error         true
           :error-message "Could not login user"})))









(defn set-user-secret-mode-for-session-id [{session-id    :session-id
                                            secret-mode   :secret-mode}]

  (let [session-request
        (sql-1 "select id, fk_ojobs_user_id from ojobs_web_sessions where id = ?"
             [session-id])

        user-request
        (sql-1 "select id, user_name, show_secret_mode from ojobs_users where id = ?"
             [(:fk_ojobs_user_id session-request)])
        ]
        (if user-request
                  (sql-1 "update ojobs_users set show_secret_mode = ? where id = ?"
             [ (if secret-mode "Y" "N")
              (:fk_ojobs_user_id session-request)  ])


          {:error         true
           :error-message "Could not set secret"})))










































"-------------------------------------------------
(create-data-session)



A server side method called from the client which
creates a new unique identifier so we can recognise
the client session
-------------------------------------------------"
(defn create-data-session []
          {:value (uuid-str)})


